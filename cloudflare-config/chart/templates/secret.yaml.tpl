apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-dns
type: Opaque
stringData:
  key: "{{ .Values.Cloudflare.Key }}"
  email: "{{ .Values.Cloudflare.Email }}"
  zone: "{{ .Values.Cloudflare.Zone }}"
  subdomain: "{{ .Values.Cloudflare.Subdomain }}"
