releases:
  - name: ingress-tests
    namespace: nginx-ingress
    chart: ./chart
    labels:
      phase: only-testing
    values:
      - domain: "{{ .Values.cluster.domain }}"
        cf_zone: "{{ .Values.cloudflare.zone }}"
        cf_key: "{{ .Values.cloudflare.key }}"
        cf_email: "{{ .Values.cloudflare.email }}"
        http_subdomain: "{{ .Values.cloudflare.subdomains.http }}"
