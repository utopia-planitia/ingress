#!/usr/bin/env bats

setup () {
  kubectl get no --no-headers | awk '{ print $1 }' | xargs kubectl uncordon
  kubectl -n nginx-ingress patch cronjobs cleanup-ingress-dns -p '{"spec" : {"suspend" : true }}'
  export CF_API_KEY={{ .Values.cf_key }}
  export CF_API_EMAIL={{ .Values.cf_email }}
}

teardown () {
  kubectl -n nginx-ingress patch cronjobs cleanup-ingress-dns -p '{"spec" : {"suspend" : false }}'

  kubectl uncordon ${NODE}
  kubectl -n nginx-ingress delete --ignore-not-found=true job/cleanup-ingress-now

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "normal life cycle" {
  # ip registered at cloudflare when pod started
  NODE=`kubectl -n nginx-ingress get po -o wide -l app.kubernetes.io/name=ingress-nginx -l app.kubernetes.io/component=controller --no-headers -o=jsonpath={.items[0].spec.nodeName}`
  POD=`kubectl -n nginx-ingress get po -o wide -l app.kubernetes.io/name=ingress-nginx -l app.kubernetes.io/component=controller --no-headers -o=jsonpath={.items[0].metadata.name}`
  run kubectl -n nginx-ingress wait --timeout=120s --for=condition=Ready pod/${POD}
  [ $status -eq 0 ]
  NODE_IP=`kubectl -n nginx-ingress exec ${POD} -c dnslb -- /dnslb ipv4`
  sleep 10
  run sh -c "flarectl dns list --zone {{ .Values.cf_zone }} --name {{ .Values.http_subdomain }}.{{ .Values.cf_zone }} | grep '${NODE_IP}'"
  [ $status -eq 0 ]

  # ip gets unregistered during pod deletion
  kubectl cordon ${NODE}
  kubectl -n nginx-ingress delete pod/${POD}
  sleep 10
  run sh -c "flarectl dns list --zone {{ .Values.cf_zone }} --name {{ .Values.http_subdomain }}.{{ .Values.cf_zone }} | grep '${NODE_IP}'"
  [ $status -eq 1 ]
}

@test "cronjob cleans up dirty shutdown" {
  # add bad ip
  flarectl dns create --zone {{ .Values.cf_zone }} --name {{ .Values.http_subdomain }}.{{ .Values.cf_zone }} --content "127.0.0.1" --type A --ttl 120

  # cronjob removes deprecated IP
  kubectl -n nginx-ingress create job cleanup-ingress-now --from=cronjob/cleanup-ingress-dns
  sleep 3
  kubectl -n nginx-ingress wait --for=condition=complete job/cleanup-ingress-now --timeout=60s
  sleep 10
  run sh -c "flarectl dns list --zone {{ .Values.cf_zone }} --name {{ .Values.http_subdomain }}.{{ .Values.cf_zone }} | grep '127.0.0.1'"
  [ $status -eq 1 ]
}
