#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "verify necessary mod security annotations" {
  run kubectl delete ingress test --ignore-not-found=true --wait=true
  [ $status -eq 0 ]

  run kubectl create ingress test --rule=foo.com/bar=svc1:8080
  [ $status -eq 0 ]

  run kubectl get ingress test -o yaml
  [ $status -eq 0 ]

  #[ "$(kubectl get ingress test -o jsonpath='{.metadata.annotations.nginx\.ingress\.kubernetes\.io\/enable-modsecurity}')" == "true" ]
  #[ "$(kubectl get ingress test -o jsonpath='{.metadata.annotations.nginx\.ingress\.kubernetes\.io\/enable-owasp-core-rules}')" == "true" ]
  #[[ "$(kubectl get ingress test -o jsonpath='{.metadata.annotations.nginx\.ingress\.kubernetes\.io\/modsecurity-snippet}')" == *"SecRuleEngine On"* ]]
  #[ "$(kubectl get ingress test -o jsonpath='{.metadata.annotations.nginx\.ingress\.kubernetes\.io\/modsecurity-transaction-id}')" == "\$request_id" ]
}
