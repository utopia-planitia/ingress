#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "try to get result from echoserver over port 80" {
  run sh -c "curl -4 -o /dev/null --max-time 30 --connect-timeout 30 --fail --silent -w \"%{http_code}\" http://echo-server.{{ .Values.domain }}"
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" = "308" ]
}

@test "try to get result from echoserver over port 443" {
  run sh -c "curl -4 -k --silent --max-time 30 --connect-timeout 30 --fail https://echo-server.{{ .Values.domain }}"
  [ $status -eq 0 ]
  [[ "${lines[0]}" =~ ^Hostname:\ .* ]]
}

@test "client IP v4 is forwarded" {
  CLIENT_IP=`curl -4 --silent --max-time 30 --connect-timeout 30 --fail checkip.amazonaws.com`
  run curl -4 --silent --max-time 30 --connect-timeout 30 --fail -k https://echo-server.{{ .Values.domain }}
  echo "${output}"
  [ $status -eq 0 ]
  run sh -c "echo \"${output}\" | grep '${CLIENT_IP//[$'\t\r\n']}'"
  echo "${output}"
  [ $status -eq 0 ]
}
