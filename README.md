# Ingress

A deployment of [Nginx Ingress](https://github.com/kubernetes/ingress-nginx)

## Usage

Available [Annotations](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/) are documented by the nginx ingress controller authors.

## Access

- HTTP on node port 80
- HTTPS on node port 443
